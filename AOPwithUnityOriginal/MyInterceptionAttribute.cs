﻿using System;

namespace AOPwithUnityOriginal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MyInterceptionAttribute : Attribute
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public MyInterceptionAttribute(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
