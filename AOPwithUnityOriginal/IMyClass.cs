﻿namespace AOPwithUnityOriginal
{
    public interface IMyClass
    {
        void Do();

        void DoAgain();
    }
}
