﻿using System;
using Microsoft.Practices.Unity.Configuration;
using Unity;

namespace AOPwithUnityOriginal
{
    static class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.LoadConfiguration();

            var myClass = container.Resolve<IMyClass>();
            myClass.Do();
            myClass.DoAgain();

            Console.ReadLine();
        }
    }
}
