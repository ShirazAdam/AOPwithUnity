﻿using System;
using AOPwithUnityOriginal;

namespace ConsoleApp1
{
    public class MyClass : IMyClass
    {
        [MyInterception(nameof(MyClass), nameof(MyClass))]
        public void Do()
        {
            Console.WriteLine("Do!");
        }

        public void DoAgain()
        {
            Console.WriteLine("Do Again!");
        }
    }
}
