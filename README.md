# AOPwithUnity
Using Unity along with Interception and Policy Injection to achieve Aspect Oriented Programming (AOP)

This project was originally inspired from https://stackoverflow.com/questions/17090512/c-sharp-unity-interception-by-attribute/17157574#17157574. however, it has been modified to support Unity v5.

AOPwithUnityOriginal is the original solution proposed in https://stackoverflow.com/questions/17090512/c-sharp-unity-interception-by-attribute/17157574#17157574 with a slight modification to support Unity v5 instead of v4.

AOPwithUnityOriginalRevised is the revision of AOPwithUnityOriginal by removing the unnecessary attribute file
