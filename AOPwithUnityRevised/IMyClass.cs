﻿namespace AOPwithUnityOriginalRevised
{
    public interface IMyClass
    {
        void Do();

        void DoAgain();
    }
}
