﻿using System;

namespace ConsoleApp2
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class MyInterceptionAttribute : Attribute
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public MyInterceptionAttribute(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
