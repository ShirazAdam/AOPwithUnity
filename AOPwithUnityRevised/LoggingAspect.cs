﻿using System;
using Unity.Interception.PolicyInjection.Pipeline;

namespace AOPwithUnityOriginalRevised
{
    //renamed from 'MyLoggingCallHandler'... other changes include 'internal sealed' from 'public' and the inheritance of 'Attribute'
    internal sealed class LoggingAspect : Attribute, ICallHandler
    {
        int ICallHandler.Order { get; set; }

        IMethodReturn ICallHandler.Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            Console.WriteLine("Begin invoke: " + input.MethodBase.Name);
            IMethodReturn result = getNext()(input, getNext);
            Console.WriteLine("End invoke: " + input.MethodBase.Name);
            return result;
        }
    }
}
