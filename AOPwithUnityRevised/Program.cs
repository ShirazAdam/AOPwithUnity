﻿using System;
using Microsoft.Practices.Unity.Configuration;
using Unity;

namespace AOPwithUnityOriginalRevised
{
    static class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.LoadConfiguration();

            //the below is an example for the demo of this software only. This is not how you should be resolving dependencies in your actual software. Otherwise, this will result in a distributed dependency on the dependency injection (DI) framework.
            var myClass = container.Resolve<IMyClass>();
            myClass.Do();
            myClass.DoAgain();

            Console.ReadLine();
        }
    }
}
