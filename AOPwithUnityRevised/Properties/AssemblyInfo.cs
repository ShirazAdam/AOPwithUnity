﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AOPwithUnityOriginalRevised")]
[assembly: AssemblyDescription("Using unity for interception and policy injection to showcase AOP capabilities")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ZeroMSN")]
[assembly: AssemblyProduct("ZeroMSN")]
[assembly: AssemblyCopyright("Copyright ©  2018 ZeroMSN")]
[assembly: AssemblyTrademark("ZeroMSN")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("68f53ab3-f551-4f91-805f-fede457e2fbc")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
[assembly: NeutralResourcesLanguage("en-GB")]

