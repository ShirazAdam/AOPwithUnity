﻿using System;

namespace AOPwithUnityOriginalRevised
{
    public class MyClass : IMyClass
    {
        [LoggingAspect]
        public void Do()
        {
            Console.WriteLine("Do!");
        }

        public void DoAgain()
        {
            Console.WriteLine("Do Again!");
        }
    }
}
